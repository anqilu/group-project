/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Implementation for DoodleBug.
 */

#include "Bug.hpp"

Bug::Bug(int rowPos, int colPos) : Critter('X', rowPos, colPos) {}

void Bug::move(int rowPos, int colPos) {
  this->rowPos = rowPos;
  this->colPos = colPos;
}