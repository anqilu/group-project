/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Implementation for Ant
 */

#include "Ant.hpp"

Ant::Ant(int rowPos, int colPos) : Critter('O', rowPos, colPos) {}

void Ant::move(int rowPos, int colPos) {
  this->rowPos = rowPos;
  this->colPos = colPos;
}