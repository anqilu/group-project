##
# Author: the members of Group 28
# Date: 11-2-2018
# Description: makefile including the compiling steps for building the program.
##

CXX = g++
CXXFLAGS = -std=c++0x

main: main.o Critter.o Ant.o Bug.o Grid.o menu.o
	${CXX} ${CXXFLAGS} -Wall main.o Critter.o Ant.o Bug.o Grid.o menu.o -o main

main.o: main.cpp
	${CXX} ${CXXFLAGS} -c main.cpp

Critter.o: Critter.cpp
	${CXX} ${CXXFLAGS} -c Critter.cpp

Ant.o: Ant.cpp
	${CXX} ${CXXFLAGS} -c Ant.cpp

Bug.o: Bug.cpp
	${CXX} ${CXXFLAGS} -c Bug.cpp
	
Grid.o: Grid.cpp
	${CXX} ${CXXFLAGS} -c Grid.cpp

menu.o: menu.cpp
	${CXX} ${CXXFLAGS} -c menu.cpp
	
clean:
	rm *.o main