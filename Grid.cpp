/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Implementation for Grid.
 */

#include "Grid.hpp"
#include <iostream>
#include "Ant.hpp"
#include "Bug.hpp"

Grid::Grid(int rowSizeIn, int colSizeIn) {
  rowSize = rowSizeIn;
  colSize = colSizeIn;

  grid = new Critter**[rowSize];
  for (int i = 0; i < rowSize; i++) {
    grid[i] = new Critter*[colSize];
  }

  for (int i = 0; i < rowSize; i++) {
    for (int j = 0; j < colSize; j++) {
      grid[i][j] = NULL;
    }
  }
}

Grid::~Grid() {
  for (int i = 0; i < rowSize; i++) {
    for (int j = 0; j < colSize; j++) {
      if (grid[i][j] != NULL) {
        delete grid[i][j];
      }
    }
  }

  for (int i = 0; i < rowSize; i++) {
    delete[] grid[i];
  }
  delete[] grid;
}

int Grid::getRowSize() { return rowSize; }

int Grid::getColSize() { return colSize; }

Critter* Grid::getGridElement(int rowPos, int colPos) {
  return grid[rowPos][colPos];
}

void Grid::setAntElementRandom() {
  while (true) {
    int i, j;
    i = rand() % rowSize;
    j = rand() % colSize;
    if (getGridElement(i, j) == NULL) {
      setGridElement(i, j, new Ant(i, j));
      return;
    }
  }
}

void Grid::setBugElementRandom() {
  while (true) {
    int i, j;
    i = rand() % rowSize;
    j = rand() % colSize;
    if (getGridElement(i, j) == NULL) {
      setGridElement(i, j, new Bug(i, j));
      return;
    }
  }
}

void Grid::setGridElement(int rowPos, int colPos, Critter* ptr) {
  grid[rowPos][colPos] = ptr;
}

void Grid::printGrid() {
  for (int i = 0; i < rowSize; i++) {
    for (int j = 0; j < colSize; j++) {
      if (grid[i][j] != NULL) {
        std::cout << grid[i][j]->getSymbol() << " ";
      } else {
        std::cout << " "
                  << " ";
      }
    }
    std::cout << std::endl;
  }
}
