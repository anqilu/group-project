/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Menu functions specification.
 */

#ifndef menu_hpp
#define menu_hpp

int collectAndValidateInput1(int min);
int collectAndValidateInput2(int min, int max);

#endif /* menu_hpp */
