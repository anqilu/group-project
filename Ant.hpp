/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Specification for Ant. Ant is a sub class of Critter.
 */

#ifndef ANT_HPP
#define ANT_HPP

#include "Critter.hpp"

class Ant : public Critter {
 private:
 public:
  Ant(int rowPos, int colPos);
  void move(int rowPos, int colPos);
};

#endif