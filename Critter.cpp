/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Implementation for Critter.
 */

#include "Critter.hpp"

Critter::Critter(char symbol, int rowPos, int colPos) {
  this->symbol = symbol;
  this->rowPos = rowPos;
  this->colPos = colPos;
}

Critter::~Critter() {}

char Critter::getSymbol() { return symbol; }

int Critter::getRowPos() { return rowPos; }

int Critter::getColPos() { return colPos; }

int Critter::getLiveDay() { return liveDay; }

int Critter::getStarveDay() { return starveDay; }

int Critter::getFlag() { return flag; }

void Critter::setRowPos(int rowPos) { this->rowPos = rowPos; }

void Critter::setColPos(int colPos) { this->colPos = colPos; }

void Critter::setLiveDay(int liveDay) { this->liveDay = liveDay; }

void Critter::setStarveDay(int starveDay) { this->starveDay = starveDay; }

void Critter::setFlag(int flag) { this->flag = flag; }