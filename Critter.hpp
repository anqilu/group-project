/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Specification for Critter.
 */

#ifndef CRITTER_HPP
#define CRITTER_HPP

class Critter {
 protected:
  char symbol;
  int rowPos;
  int colPos;
  int liveDay = 0;
  int starveDay = 0;
  int flag = 0;

 public:
  Critter(char symbol, int rowPos, int colPos);

  virtual ~Critter();
  virtual void move(int rowPos, int colPos) = 0;

  char getSymbol();

  int getRowPos();
  int getColPos();
  int getLiveDay();
  int getStarveDay();
  int getFlag();

  void setRowPos(int rowPos);
  void setColPos(int colPos);
  void setLiveDay(int liveDay);
  void setStarveDay(int starveDay);
  void setFlag(int flag);
};

#endif
