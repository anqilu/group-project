/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Menu implementation, include generic input validation functions.
 */

#include "menu.hpp"
#include <iostream>
#include <limits>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int collectAndValidateInput1(int min) {
  while (true) {
    string input;
    std::getline(cin, input);

    if (!input.empty() &&
        input.find_first_not_of("0123456789") == std::string::npos) {
      int num = atoi(input.c_str());

      if (num >= min) {
        return num;
      }
    }

    cout << input << " is not a valid input" << endl;
  }
}

int collectAndValidateInput2(int min, int max) {
  while (true) {
    string input;
    std::getline(cin, input);

    if (!input.empty() &&
        input.find_first_not_of("0123456789") == std::string::npos) {
      int num = atoi(input.c_str());

      if (num >= min && num <= max) {
        return num;
      }
    }

    cout << input << " is not a valid input" << endl;
  }
}
