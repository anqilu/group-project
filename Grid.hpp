/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Specification for Grid.
 */

#ifndef GRID_HPP
#define GRID_HPP

#include "Critter.hpp"

class Grid {
 private:
  int rowSize;
  int colSize;
  Critter*** grid;

 public:
  Grid(int rowSizeIn, int colSizeIn);
  ~Grid();

  int getRowSize();
  int getColSize();
  Critter* getGridElement(int rowPos, int colPos);

  void setGridElement(int rowPos, int colPos, Critter* ptr);
  void setAntElementRandom();
  void setBugElementRandom();

  void printGrid();
};

#endif