/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Specification for DoodleBug. DoodleBug is a sub class for
 * Critter
 */

#ifndef BUG_HPP
#define BUG_HPP

#include "Critter.hpp"

class Bug : public Critter {
 private:
 public:
  Bug(int rowPos, int colPos);
  void move(int rowPos, int colPos);
};

#endif