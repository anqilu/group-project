/**
 * Author: the members of Group 28
 * Date: 11-2-2018
 * Description: Program entry point, contains the "main" function.
 */

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <sstream>
#include "Ant.hpp"
#include "Bug.hpp"
#include "Critter.hpp"
#include "Grid.hpp"
#include "menu.hpp"

int main() {
  std::cout << "Extra Credit: in addition to prompting the user for the number "
               "of time steps,"
            << std::endl;
  std::cout << "ask them to enter the size of the grid rows and columns, the "
               "number of ants, and the number of doodlebugs."
            << std::endl;
  int rowSize;  // grid row size
  int colSize;  // grid col size
  int antNum;   // ant number
  int bugNum;   // bug number
  int steps;    // simulation steps

  int input;
  int check = 1;

  // loop circling untill user opt to quit program
  while (check != 0) {
    // prompt user to specify simulation parameters
    std::cout << "simulation steps (e.g. steps = 50):" << std::endl;
    steps = collectAndValidateInput1(1);

    std::cout << "grid row size (e.g. rowSize = 20):" << std::endl;
    rowSize = collectAndValidateInput1(1);

    std::cout << "grid column size (e.g. colSize = 20):" << std::endl;
    colSize = collectAndValidateInput1(1);

    std::cout << "ant number (e.g. antNum = 100):" << std::endl;
    antNum = collectAndValidateInput1(0);

    std::cout << "bug number (e.g. bugNum = 5):" << std::endl;
    bugNum = collectAndValidateInput1(0);

    // randomly place ants and bugs on the grid
    Grid grid(rowSize, colSize);
    Critter* ptr;
    srand(time(NULL));
    int count;
    count = 0;
    while (count < antNum) {
      grid.setAntElementRandom();
      count++;
    }
    count = 0;
    while (count < bugNum) {
      grid.setBugElementRandom();
      count++;
    }

    std::cout << std::endl;
    grid.printGrid();

    // Simulation begins
    for (int iter = 1; iter <= steps; iter++) {
      // Move step for bug
      for (int i = 0; i < rowSize; i++) {
        for (int j = 0; j < colSize; j++) {
          // locate a bug
          if (grid.getGridElement(i, j) != NULL &&
              grid.getGridElement(i, j)->getFlag() == 0 &&
              grid.getGridElement(i, j)->getSymbol() == 'X') {
            // std::cout << "It is a bug" << std::endl;
            // set flag to not allow critter to make two moves
            grid.getGridElement(i, j)->setFlag(1);
            int haveEaten = 0;
            int temp1, k, i2, j2;
            temp1 = rand() % 4;
            for (int temp2 = 0; temp2 < 4; temp2++) {
              k = (temp1 + temp2) % 4;  // 0. up; 1. down; 2. left; 3. right;
              switch (k) {
                case 0: {
                  i2 = i - 1;
                  j2 = j;
                  break;
                }
                case 1: {
                  i2 = i + 1;
                  j2 = j;
                  break;
                }
                case 2: {
                  i2 = i;
                  j2 = j - 1;
                  break;
                }
                case 3: {
                  i2 = i;
                  j2 = j + 1;
                  break;
                }
              }
              if (i2 >= 0 && i2 <= rowSize - 1 && j2 >= 0 &&
                  j2 <= colSize - 1) {
                if (grid.getGridElement(i2, j2) != NULL &&
                    grid.getGridElement(i2, j2)->getSymbol() == 'O' &&
                    haveEaten == 0) {
                  delete grid.getGridElement(i2, j2);
                  grid.setGridElement(i2, j2, grid.getGridElement(i, j));
                  grid.setGridElement(i, j, NULL);
                  grid.getGridElement(i2, j2)->move(i2, j2);
                  grid.getGridElement(i2, j2)->setStarveDay(0);
                  haveEaten = 1;
                  break;
                }
              }
            }
            if (haveEaten == 0)  // if no ants in the adjacent cells
            {
              k = rand() % 4;  // 0. up; 1. down; 2. left; 3. right;
              switch (k) {
                case 0: {
                  i2 = i - 1;
                  j2 = j;
                  break;
                }
                case 1: {
                  i2 = i + 1;
                  j2 = j;
                  break;
                }
                case 2: {
                  i2 = i;
                  j2 = j - 1;
                  break;
                }
                case 3: {
                  i2 = i;
                  j2 = j + 1;
                  break;
                }
              }
              if (i2 >= 0 && i2 <= rowSize - 1 && j2 >= 0 &&
                  j2 <= colSize - 1) {
                if (grid.getGridElement(i2, j2) == NULL) {
                  grid.setGridElement(i2, j2, grid.getGridElement(i, j));
                  grid.setGridElement(i, j, NULL);
                  grid.getGridElement(i2, j2)->move(i2, j2);
                }
              }
            }
          }
        }
      }

      // Move step for ant
      for (int i = 0; i < rowSize; i++) {
        for (int j = 0; j < colSize; j++) {
          // locate an ant
          if (grid.getGridElement(i, j) != NULL &&
              grid.getGridElement(i, j)->getFlag() == 0 &&
              grid.getGridElement(i, j)->getSymbol() == 'O') {
            // std::cout << "It is an ant" << std::endl;
            // set flag to not allow critter to make two moves
            grid.getGridElement(i, j)->setFlag(1);
            int k, i2, j2;
            k = rand() % 4;  // 0. up; 1. down; 2. left; 3. right;
            switch (k) {
              case 0: {
                i2 = i - 1;
                j2 = j;
                break;
              }
              case 1: {
                i2 = i + 1;
                j2 = j;
                break;
              }
              case 2: {
                i2 = i;
                j2 = j - 1;
                break;
              }
              case 3: {
                i2 = i;
                j2 = j + 1;
                break;
              }
            }
            if (i2 >= 0 && i2 <= rowSize - 1 && j2 >= 0 && j2 <= colSize - 1) {
              if (grid.getGridElement(i2, j2) == NULL) {
                grid.setGridElement(i2, j2, grid.getGridElement(i, j));
                grid.setGridElement(i, j, NULL);
                grid.getGridElement(i2, j2)->move(i2, j2);
              }
            }
          }
        }
      }

      // Breed step for bug and ant
      for (int i = 0; i < rowSize; i++) {
        for (int j = 0; j < colSize; j++) {
          // locate a bug
          if (grid.getGridElement(i, j) != NULL &&
              grid.getGridElement(i, j)->getSymbol() == 'X' &&
              grid.getGridElement(i, j)->getLiveDay() >= 8) {
            // std::cout << "It is a bug" << std::endl;
            int temp1, k, i2, j2;
            temp1 = rand() % 4;
            for (int temp2 = 0; temp2 < 4; temp2++) {
              k = (temp1 + temp2) % 4;  // 0. up; 1. down; 2. left; 3. right;
              switch (k) {
                case 0: {
                  i2 = i - 1;
                  j2 = j;
                  break;
                }
                case 1: {
                  i2 = i + 1;
                  j2 = j;
                  break;
                }
                case 2: {
                  i2 = i;
                  j2 = j - 1;
                  break;
                }
                case 3: {
                  i2 = i;
                  j2 = j + 1;
                  break;
                }
              }
              if (i2 >= 0 && i2 <= rowSize - 1 && j2 >= 0 &&
                  j2 <= colSize - 1) {
                if (grid.getGridElement(i2, j2) == NULL) {
                  ptr = new Bug(i2, j2);
                  grid.setGridElement(i2, j2, ptr);
                  grid.getGridElement(i, j)->setLiveDay(0);
                  break;
                }
              }
            }
          }

          // locate an ant
          if (grid.getGridElement(i, j) != NULL &&
              grid.getGridElement(i, j)->getSymbol() == 'O' &&
              grid.getGridElement(i, j)->getLiveDay() >= 3) {
            // std::cout << "It is an ant" << std::endl;
            int temp1, k, i2, j2;
            temp1 = rand() % 4;
            for (int temp2 = 0; temp2 < 4; temp2++) {
              k = (temp1 + temp2) % 4;  // 0. up; 1. down; 2. left; 3. right;
              switch (k) {
                case 0: {
                  i2 = i - 1;
                  j2 = j;
                  break;
                }
                case 1: {
                  i2 = i + 1;
                  j2 = j;
                  break;
                }
                case 2: {
                  i2 = i;
                  j2 = j - 1;
                  break;
                }
                case 3: {
                  i2 = i;
                  j2 = j + 1;
                  break;
                }
              }
              if (i2 >= 0 && i2 <= rowSize - 1 && j2 >= 0 &&
                  j2 <= colSize - 1) {
                if (grid.getGridElement(i2, j2) == NULL) {
                  ptr = new Ant(i2, j2);
                  grid.setGridElement(i2, j2, ptr);
                  grid.getGridElement(i, j)->setLiveDay(0);
                  break;
                }
              }
            }
          }
        }
      }

      // Starve step for bug
      for (int i = 0; i < rowSize; i++) {
        for (int j = 0; j < colSize; j++) {
          // locate a bug
          if (grid.getGridElement(i, j) != NULL &&
              grid.getGridElement(i, j)->getSymbol() == 'X') {
            grid.getGridElement(i, j)->setStarveDay(
                grid.getGridElement(i, j)->getStarveDay() + 1);
            if (grid.getGridElement(i, j)->getStarveDay() >= 3) {
              delete grid.getGridElement(i, j);
              grid.setGridElement(i, j, NULL);
            }
          }
        }
      }

      std::cout << "Step ---------- " << iter << std::endl;
      grid.printGrid();

      // reset flag to allow critter to move again for next round, also inrement
      // liveDay
      for (int i = 0; i < rowSize; i++) {
        for (int j = 0; j < colSize; j++) {
          if (grid.getGridElement(i, j) != NULL) {
            grid.getGridElement(i, j)->setFlag(0);
            grid.getGridElement(i, j)->setLiveDay(
                grid.getGridElement(i, j)->getLiveDay() + 1);
          }
        }
      }
    }

    std::cout << "1. simulate again" << std::endl;
    std::cout << "2. quite" << std::endl;
    input = collectAndValidateInput2(1, 2);
    if (input == 2) {
      check = 0;
    }
  }

  return 0;
}
